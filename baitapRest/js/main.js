function tinhDiemTrungBinh(...restPara) {
  let tongDiem = 0;
  let diemTB = 0;
  for (let i = 0; i < restPara.length; i++) {
    tongDiem += restPara[i];
  }
  diemTB = tongDiem / restPara.length;
  return diemTB;
}
document.getElementById("btnKhoi1").onclick = function () {
  let diemToan1 = document.getElementById("inpToan").value * 1;
  let diemLy1 = document.getElementById("inpLy").value * 1;
  let diemHoa1 = document.getElementById("inpHoa").value * 1;
  let diemTB = tinhDiemTrungBinh(diemToan1, diemLy1, diemHoa1);
  document.getElementById("tbKhoi1").innerHTML = diemTB;
};
document.getElementById("btnKhoi2").onclick = function () {
  let diemVan2 = document.getElementById("inpVan").value * 1;
  let diemSu2 = document.getElementById("inpSu").value * 1;
  let diemDia2 = document.getElementById("inpDia").value * 1;
  let diemEnglish2 = document.getElementById("inpEnglish").value * 1;
  let diemTB = tinhDiemTrungBinh(diemVan2, diemSu2, diemDia2, diemEnglish2);
  document.getElementById("tbKhoi2").innerHTML = diemTB;
};
