let btnContainer = document.getElementById("colorContainer");
let btns = btnContainer.getElementsByClassName("color-button");
let house = document.getElementById("house");
let originHouse = document.getElementById("house").className;
const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

loadColor(colorList);
activeColor(btns);
changeHouse(btns, house, colorList, originHouse);
