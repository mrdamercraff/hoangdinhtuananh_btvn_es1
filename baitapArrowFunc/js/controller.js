let loadColor = (colorList) => {
  let innerHTML = "";
  colorList.forEach((color, index) => {
    let innerButton = "";
    if (index == 0) {
      innerButton = `<button class="color-button ${color} active"></button>`;
      innerHTML += innerButton;
      return;
    }
    innerButton = `<button class="color-button ${color}"></button>`;
    innerHTML += innerButton;
  });
  document.getElementById("colorContainer").innerHTML = innerHTML;
};
let activeColor = (btns) => {
  for (let i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function () {
      let current = document.getElementsByClassName("active");
      current[0].className = current[0].className.replace(" active", "");
      this.className += " active";
      changeHouse(btns, house, colorList, originHouse);
    });
  }
};
let findIndex = (btns) => {
  for (let i = 0; i < btns.length; i++) {
    if (btns[i].classList.contains("active")) {
      return i;
    }
  }
};
let changeHouse = (btns, house, colorList, originHouse) => {
  let i = findIndex(btns);
  house.className = originHouse;
  house.classList.add(`${colorList[i]}`);
};
